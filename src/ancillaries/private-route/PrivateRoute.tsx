import React, { ElementType, FC, useContext } from 'react';
import { Redirect, Route, RouteProps } from 'react-router-dom';

import AuthContext from 'utils/auth.context';

export interface PrivateRouteProps extends RouteProps {}

export const PrivateRoute: FC<PrivateRouteProps> = ({ component, ...rest }) => {
  const Component = component as ElementType;
  const { isLogin } = useContext(AuthContext);
  const isToken = localStorage.getItem('token');

  return (
    <Route
      {...rest}
      render={(props) =>
        isLogin || (isToken && isToken.length > 0) ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: '/login',
              state: { from: props.location },
            }}
          />
        )
      }
    />
  );
};
