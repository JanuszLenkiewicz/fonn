import axios, { AxiosRequestConfig } from 'axios';
import { URL_base_API } from 'const/global';

export const axiosInstance = axios.create({
  baseURL: URL_base_API,
  headers: {
    Accept: 'application/json, text/plain, */*',
    'Content-Type': 'application/json',
  } as AxiosRequestConfig,
});
