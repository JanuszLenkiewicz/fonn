import { axiosInstance } from 'services/_axios.instance';

export const postLogin = (username: string, password: string) => {
  const data = JSON.stringify({
    username: username,
    password: password,
  });
  return axiosInstance.post('login', data);
};
