import { createContext } from 'react';

export interface Auth {
  isLogin: boolean;
  username: string;
  setIsLogin: any;
  setUsername: any;
  logout: any;
}

const AuthContext = createContext<Auth>({
  isLogin: false,
  username: '',
  setIsLogin: null,
  setUsername: null,
  logout: null,
} as Auth);

export default AuthContext;
