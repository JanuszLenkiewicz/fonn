import React, { useContext } from 'react';
import { useHistory } from 'react-router-dom';
import { Navbar, NavbarBrand, Nav, NavbarText, Button } from 'reactstrap';

import AuthContext from 'utils/auth.context';

const NavbarComponent = () => {
  const history = useHistory();
  const { logout } = useContext(AuthContext);

  const logoutHandler = () => {
    logout();
    history.push('/login');
  };

  return (
    <AuthContext.Consumer>
      {({ isLogin, username, logout }) => (
        <Navbar
          color="light"
          light
          expand="md"
          className="d-flex justify-content-between px-3"
        >
          <NavbarBrand className="ml-1" href="/">
            recruitmentTask
          </NavbarBrand>
          <div className="d-flex justify-content-between">
            <NavbarText className="px-3">
              {isLogin && `Hello ${username}!`}
            </NavbarText>
            <Nav className="mr-auto" navbar>
              <Button onClick={logoutHandler} outline color="secondary">
                Logout
              </Button>
            </Nav>
          </div>
        </Navbar>
      )}
    </AuthContext.Consumer>
  );
};

export default NavbarComponent;
