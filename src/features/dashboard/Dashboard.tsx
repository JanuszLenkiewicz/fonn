import React from 'react';
import NavbarComponent from 'features/dashboard/components/NavbarComponent';

const MyComponent = () => {
  return (
    <div>
      <NavbarComponent />
      <div className="wrapper">Dashboard</div>
    </div>
  );
};

export default MyComponent;
