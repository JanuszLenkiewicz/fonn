import React, { useState, useContext, FormEvent } from 'react';
import { useHistory } from 'react-router-dom';
import { useSnackbar } from 'notistack';

import { postLogin } from 'services/user.service';
import AuthContext from 'utils/auth.context';

const LoginForm = () => {
  const { enqueueSnackbar } = useSnackbar();
  const authContext = useContext(AuthContext);
  const history = useHistory();

  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const submit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    postLogin(username, password)
      .then((data: any) => {
        localStorage.setItem('token', data.data.token);
        enqueueSnackbar('Success!', {
          variant: 'success',
        });
        authContext.setIsLogin(true);
        authContext.setUsername(username);
        history.push('/');
      })
      .catch((err) => {
        enqueueSnackbar('Bad login or password', {
          variant: 'error',
        });
      });
  };

  return (
    <div className="container">
      <form className="form" onSubmit={submit}>
        <div>
          <input
            type="text"
            placeholder="Username"
            value={username}
            onChange={(e) => setUsername(e.target.value)}
          />
        </div>
        <div>
          <input
            type="password"
            placeholder="Password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </div>
        <div>
          <button type="submit">Sign in</button>
        </div>
      </form>
    </div>
  );
};

export default LoginForm;
