import React, { useState } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import LoginForm from 'features/login/LoginForm';
import Dashboard from 'features/dashboard/Dashboard';
import AuthContext from 'utils/auth.context';
import { PrivateRoute } from 'ancillaries/private-route/PrivateRoute';

function App() {
  const [username, setUsername] = useState('');
  const [isLogin, setIsLogin] = useState(false);

  const logout = () => {
    setIsLogin(false);
    localStorage.setItem('token', '');
    setUsername('');
  };

  return (
    <div className="App">
      <BrowserRouter>
        <AuthContext.Provider
          value={{ username, isLogin, setUsername, setIsLogin, logout }}
        >
          <Switch>
            <PrivateRoute exact path="/" component={Dashboard} />
            <Route path="/login" component={LoginForm} />
          </Switch>
        </AuthContext.Provider>
      </BrowserRouter>
    </div>
  );
}

export default App;
